import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
public class FileSize {
	 public static void main(String[] args) throws FileNotFoundException {
		 //how to compute directory size
		 File f = new File(System.getProperty("user.home").split(":")[0]); //computing home directory size
		 //System.out.println(System.getProperty("user.home").split(":")[0]);
		 System.out.println("Total Space");
		 System.out.println(f.getTotalSpace() +" bytes");
		 System.out.println(f.getTotalSpace()/1000.00 +" Kilobytes");
		 System.out.println(f.getTotalSpace()/1000000.00 +" Megabytes");
		 System.out.println(f.getTotalSpace()/1000000000.00 +" Gigabytes");
		 System.out.println("----------------------------");
		 System.out.println("Free Space available");
		 System.out.println(f.getFreeSpace() +" bytes");
		 System.out.println(f.getFreeSpace()/1000.00 +" Kilobytes");
		 System.out.println(f.getFreeSpace()/1000000.00 +" Megabytes");
		 System.out.println(f.getFreeSpace()/1000000000.00 +" Gigabytes");
		 
		 System.out.println("----------------------------");
		 
		 //How to compute file size
		 File file = new File("F:\\Travel Reservation July 14 for AHMED.pdf");
		 System.out.println("Size of pdf file");
		 System.out.println(file.length() + "bytes");
		 System.out.println(file.length()/1000.0 + " Kilobytes");
		 System.out.println(file.getFreeSpace()/1000000.00 +" Megabytes");
		 System.out.println(file.getFreeSpace()/1000000000.00 +" Gigabytes");
		 
		 System.out.println("----------------------------");
		 
		 //Creating directory(folder) 
		 boolean flag = false;
		 File dir = new File ("F:\\test");
		 try{
			 flag = dir.mkdir();
		 }catch(SecurityException e){
			 System.out.println("Error creating directory");
		 }
		 
		 if(flag){
			 System.out.println("Successfull created directory");
		 }
		 else {
			 System.out.println("Directory not created");
		 }
		 
		 System.out.println("----------------------------");
		 
		 //Storing a file to a specific directory
		 File file1 = new File("D:\\Test.txt"); //destination and target file name
		 OutputStream out = new FileOutputStream(file1);	 
		 byte[] data = null;
		 
		 //location of file to be copied
		 Path path = Paths.get("F:\\working Digraph.txt");//source directory and file name
		 try {
			data = Files.readAllBytes(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
		 try {
			out.write(data);
			out.close();
			System.out.println("Successfull written to file");
		} catch (IOException e) {
			System.out.println("Failed to write data");
			e.printStackTrace();
		}
		 				 
	 }//main
	 
}//Class
