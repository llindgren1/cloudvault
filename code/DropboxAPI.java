// Include the Dropbox SDK.
import com.dropbox.core.*;

import java.awt.Desktop;
import java.io.*;
import java.net.URL;
import java.util.Locale;

public class DropboxAPI {
	/**
	 private static final String APP_KEY = "your application key";  
	 private static final String APP_SECRET = "application secret"; 
	 private static final AccessType ACCESS_TYPE = AccessType.APP_FOLDER;  **/

    public static void main(String[] args) throws IOException, DbxException, Exception {
    	
    	
        // Get your app key and secret from the Dropbox developers website.
        final String APP_KEY = "ye2kftwgryzv60i";
        final String APP_SECRET = "2jl7uvrkgnnrwsd";

        DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);

        DbxRequestConfig config = new DbxRequestConfig("JavaTutorial/1.0", Locale.getDefault().toString());
        System.out.println("Locale: " + Locale.getDefault().toString());
        DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);

       
        // Have the user sign in and authorize your app.
        String authorizeUrl = webAuth.start(); //generates an authorization url
        
//        Desktop.getDesktop().browse(new URL(authorizeUrl).toURI()); //opening browser and directing user to url
        
        //helping instructions for user
        System.out.println("1. Go to: " + authorizeUrl);
        System.out.println("2. Click Allow (you might have to log in first)");
        System.out.println("3. Copy the authorization code.");
        
        //code needed to create token
        String code = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();

        // This will fail if the user enters an invalid authorization code.
        
        //DbxAuthFinish authFinish = webAuth.finish(code);

        //String token = authFinish.accessToken; //creates the access token to make API calls on user's behalf
	String token = "CFD7NJ4BxqIAAAAAAAAAAUCwsFQK2QW2Wc7izlqFX0rtsF162vQgUQCJaYw69vqL";
        System.out.println("Token is: " + token);
        
        //DbxClient client = new DbxClient(config, authFinish.accessToken);
        DbxClient client = new DbxClient(config, token);
        
        //keeping the authorization information for future use line 55
        //File argAuthFileOutput = new File("D:\\info.txt"); 
        File argAuthFileOutput = new File("/Users/chrono/info.txt"); 
       
        // Save auth information to output file.
        DbxAuthInfo authInfo = new DbxAuthInfo(token, appInfo.host);
        
        try {
            DbxAuthInfo.Writer.writeToFile(authInfo, argAuthFileOutput);
            System.out.println("Saved authorization information to \"" + argAuthFileOutput + "\".");
        }
        catch (IOException ex) {
            System.err.println("Error saving to <auth-file-out>: " + ex.getMessage());
            System.err.println("Dumping to stderr instead:");
            DbxAuthInfo.Writer.writeToStream(authInfo, System.out);
            System.exit(1); return;
        } 
                
        System.out.println("Linked account: " + client.getAccountInfo().displayName);

        File inputFile = new File("/Users/chrono/dropbox.sh");
        FileInputStream inputStream = new FileInputStream(inputFile);
        try {
            DbxEntry.File uploadedFile = client.uploadFile("/myfile1.pdf",
                DbxWriteMode.add(), inputFile.length(), inputStream);
            System.out.println("Uploaded: " + uploadedFile.toString());
        } finally {
            inputStream.close();
        }

        DbxEntry.WithChildren listing = client.getMetadataWithChildren("/");
        System.out.println("Files in the root path:");
        for (DbxEntry child : listing.children) {
            System.out.println("	" + child.name + ": " + child.toString());
        }

        //FileOutputStream outputStream = new FileOutputStream("D:\\newfile.pdf");
        FileOutputStream outputStream = new FileOutputStream("/Users/chrono/dropbox.sh.new");
        try {
            DbxEntry.File downloadedFile = client.getFile("/myfile1.pdf", null,
                outputStream);
            System.out.println("Metadata: " + downloadedFile.toString());
        } finally {
            outputStream.close();
        }
        
    }
       
}
