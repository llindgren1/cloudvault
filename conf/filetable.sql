create table file (
fid integer primary key autoincrement,
filename text not null unique,
filesize int not null,
last_modified int not null,
hash text not null
);

create table chunks (
cid integer primary key autoincrement,
filename text not null,
sid int not null,
chunk text not null unique,
num int not null
);
