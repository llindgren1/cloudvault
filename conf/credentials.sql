create table credentials (
id integer primary key autoincrement,
sid integer not null,
name text not null,
token text not null,
key text not null
);
