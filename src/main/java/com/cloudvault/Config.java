/*
 * CloudVault.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-439  -  DISTRIBUTED SYSTEMS  -  FALL  2013
 *
 * Lars J. Lindgren <chrono@eeky.net>
 * Xiying Deng <xdeng1@luc.edu>
 * Zain Maqsood <zain.maqsood01@ymail.com>
 *
 */

package com.cloudvault;
 
public class Config {
	public final static String CLOUD_VAULT_IDENTIFIER = "CloudVault/1.0";
	public final static String CLOUD_VAULT_DIR = System.getProperty("user.home") + "/.cloudvault";
	public final static String SPLIT_DIR = CLOUD_VAULT_DIR + "/split";
	public final static String MERGE_DIR = CLOUD_VAULT_DIR + "/merge";
	public final static String CREDENTIALS = CLOUD_VAULT_DIR + "/credentials";
	public final static String CREDENTIALS_DB = CLOUD_VAULT_DIR + "/credentials.db";
	public final static String FILE_TABLE = CLOUD_VAULT_DIR + "/filetable.db";

	public final static int SRV_LOCAL = 0;
	public final static int SRV_DROPBOX = 1;
	public final static int SRV_GDRIVE = 2;
}
