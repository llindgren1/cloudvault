/*
 * StorageFactory.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-439  -  DISTRIBUTED SYSTEMS  -  FALL  2013
 *
 * Lars J. Lindgren <chrono@eeky.net>
 * Xiying Deng <xdeng1@luc.edu>
 * Zain Maqsood <zain.maqsood01@ymail.com>
 *
 */

package com.cloudvault.storageservices;

import com.cloudvault.Config;
import com.cloudvault.Credentials;
import com.cloudvault.storageservices.*;

import java.util.ArrayList;

public class StorageFactory {
	public StorageFactory() { }

	public ArrayList<StorageService> getStorageServices(ArrayList<Credentials> credentials) {
                ArrayList<StorageService> ssrvs = new ArrayList<StorageService>();
                for (Credentials c : credentials) ssrvs.add(c.getId(), create(c));

		return ssrvs;
        }

	public StorageService create(Credentials c) {
		switch (c.getStorageId()) {
			case Config.SRV_LOCAL:
				return new LocalStorage(c);
			case Config.SRV_DROPBOX:
				return new DropboxStorage(c);
			case Config.SRV_GDRIVE:
				return new GoogleDriveStorage(c);
		}

		return null;
	}	
}
