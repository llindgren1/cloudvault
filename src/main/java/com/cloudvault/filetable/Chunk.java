/*
 * Chunk.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-439  -  DISTRIBUTED SYSTEMS  -  FALL  2013
 *
 * Lars J. Lindgren <chrono@eeky.net>
 * Xiying Deng <xdeng1@luc.edu>
 * Zain Maqsood <zain.maqsood01@ymail.com>
 *
 */

package com.cloudvault.filetable;

public class Chunk {

	private int storageId;
	private String fileName;
	private String chunkName;
	private int number;

	public Chunk(int storageId, String fileName, String chunkName, int number) {
		this.storageId = storageId;
		this.fileName = fileName;
		this.chunkName = chunkName;
		this.number = number;
	}

	public int getStorageId() { return this.storageId; }

	public String getFileName() { return this.fileName; }
	public String getChunkName() { return this.chunkName; }
	public int getNumber() { return this.number; }

	public String toString() {
		return String.format("%d:%s:%s:%d", storageId, chunkName, fileName, number);
	}
}
