/*
 * EpochTest.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-439  -  DISTRIBUTED SYSTEMS  -  FALL  2013
 *
 * Lars J. Lindgren <chrono@eeky.net>
 * Xiying Deng <xdeng1@luc.edu>
 * Zain Maqsood <zain.maqsood01@ymail.com>
 *
 */

package com.cloudvault.test;

import org.junit.Assert;
import org.junit.Test;

import com.cloudvault.Epoch;

/**
 * Tests for {@link Epoch}.
 *
 * @author llindgren1@luc.edu (Lars Lindgren)
 */
public class EpochTest {
	
	@Test
	public void getNow() {
		Assert.assertTrue(Epoch.getNow() > 0);
	}

	@Test
	public void epochToString() {
		Assert.assertEquals(Epoch.toString(1384491231), "Thu Nov 14 22:53:51 CST 2013");
	}

	@Test
	public void stringToEpoch() {
		Assert.assertTrue(Epoch.toEpoch("Thu Nov 14 22:53:51 CST 2013") == 1384491231);
	}
}
